﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player2AttackPointManager : MonoBehaviour
{
    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        if (other.name != "Player2" && other.name != "Player2(Clone)" && other.tag == "RedTeam")
        {
            Destroy(this.gameObject);
        }
    }
}
