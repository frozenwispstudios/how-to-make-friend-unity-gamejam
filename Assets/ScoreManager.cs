﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class ScoreManager : MonoBehaviour
{
    //get score text
    [SerializeField] private TextMeshProUGUI BlueScoreText;
    [SerializeField] private TextMeshProUGUI RedScoreText;
    [SerializeField] private TextMeshProUGUI LevelTimerText;

    [SerializeField] private GameObject WinsScreen;
    [SerializeField] private TextMeshProUGUI WinScreenText;

    private int RedTeamKingScore = 0;
    private int BlueTeamKingScore = 0;

    private float startTime = 60*5;
    private float currentTime = 0;

    // Start is called before the first frame update
    void Start()
    {
        currentTime = startTime;

        BlueScoreText.text = "Score: " + BlueTeamKingScore;
        RedScoreText.text = "Score: " + RedTeamKingScore;
        LevelTimerText.text = "Time: " + Mathf.Floor(currentTime / 60) +":00";

        StartCoroutine(PresecondTimer());
    }

    // Update is called once per frame
    void Update()
    {
        BlueScoreText.text = "Score: " + BlueTeamKingScore;
        RedScoreText.text = "Score: " + RedTeamKingScore;
        

      //  print("min" + Mathf.Floor(currentTime / 60) +":"+ (currentTime % 60));
     //   print("test" + currentTime);

        WinScreen();
    }

    public void UpdateKingPoints(string _Colour, int _KingScore)
    {
        if (_Colour == "Red")
        {
            RedTeamKingScore += _KingScore;
        }else if (_Colour == "Blue")
        {
            BlueTeamKingScore += _KingScore;
        }
    }

    public void WinScreen()
    {
        if (currentTime <= 0)
        {
            WinsScreen.SetActive(true);
            if (RedTeamKingScore > BlueTeamKingScore) { WinScreenText.text = "Red Wins"; }
            else if (RedTeamKingScore < BlueTeamKingScore) { WinScreenText.text = "Blue Wins"; }

            if (Input.GetButtonDown("Melee1") || Input.GetButtonDown("Melee2"))
            {
                SceneManager.LoadScene("StartScene");
            }
        }

        
    }



    public float GetTime()
    {
        return currentTime;
    }

    IEnumerator PresecondTimer()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);
            currentTime -= 1;
            LevelTimerText.text = "Time: " + Mathf.Floor(currentTime / 60) + ":" + (currentTime % 60);
        }
    }
}
