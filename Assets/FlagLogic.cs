﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlagLogic : MonoBehaviour
{
    [SerializeField] GameObject Flagbody;
    [SerializeField] GameObject FlagTip;

    private Renderer FlagbodyColour;
    private Renderer FlagTipColour;

    [SerializeField] Material blueTeamMat;
    [SerializeField] Material redTeamMat;
    [SerializeField] Material defaultMat;

    [SerializeField] string teamColour = "NULL";

    GameObject scoreManagerObject;

    private string captureColour = "NULL";

    private float blueFlagPoints = 0f;
    private float redFlagPoints = 0f;
    private float neutralFlagPoints = 100f;

    private float maxCapturePoints = 100f;

    private float flagCaptureSpeed = 60;

    private bool both = false;
    public bool redPresent = false;
    public bool bluePresent = false;

    // Start is called before the first frame update
    void Start()
    {
        FlagbodyColour = Flagbody.GetComponent<Renderer>();
        FlagbodyColour.enabled = true;
        FlagbodyColour.sharedMaterial = defaultMat;

        FlagTipColour = FlagTip.GetComponent<Renderer>();
        FlagTipColour.enabled = true;
        FlagTipColour.sharedMaterial = defaultMat;

        StartCoroutine(WhitePointsUpdate());
        StartCoroutine(AddScoreToteamColour());

        scoreManagerObject = GameObject.Find("ScoreManager");
    }

    private void Update()
    {
        //check the distance of the player then 
        UpdateFlagColour();
        UpdateNeutral();
        UpdatePoints();
    }

    private void UpdateFlagColour()
    {
        if (redFlagPoints >= 100)
        {
            FlagbodyColour.sharedMaterial = redTeamMat;
            FlagTipColour.sharedMaterial = redTeamMat;
            captureColour = "Red";
        }
        else if (blueFlagPoints >= 100)
        {
            FlagbodyColour.sharedMaterial = blueTeamMat;
            FlagTipColour.sharedMaterial = blueTeamMat;
            captureColour = "Blue";
        }
        else if (neutralFlagPoints > 1)
        {
            FlagbodyColour.sharedMaterial = defaultMat;
            FlagTipColour.sharedMaterial = defaultMat;
            captureColour = "NULL";
        }
    }

    private void UpdateNeutral()
    {
        //cap white /recap blue or red
      //  print("red" + redFlagPoints);
       // print("blue" + blueFlagPoints);
       // print("white" + neutralFlagPoints);
    }

    private void UpdatePoints()
    {
        if (redPresent && !bluePresent)
        {
            //red stuff
            if (neutralFlagPoints > 0)
            {
                neutralFlagPoints -= Time.deltaTime * flagCaptureSpeed;
            }
            else if (blueFlagPoints > 0)
            {
                //-= blue
                blueFlagPoints -= Time.deltaTime * flagCaptureSpeed;
            }
            else
            {
                if (redFlagPoints < maxCapturePoints)
                {
                    if (redFlagPoints > maxCapturePoints)
                    {
                        redFlagPoints = maxCapturePoints;
                    }
                    else
                    {
                        redFlagPoints += Time.deltaTime * flagCaptureSpeed;
                    }
                }
            }
        }

        if (bluePresent && !redPresent)
        {
            //blue stuff
            if (neutralFlagPoints > 0)
            {
                neutralFlagPoints -= Time.deltaTime * flagCaptureSpeed;
            }
            else if (redFlagPoints > 0)
            {
                //-= blue
                redFlagPoints -= Time.deltaTime * flagCaptureSpeed;
            }
            else
            {
                if (blueFlagPoints < maxCapturePoints)
                {
                    if (blueFlagPoints > maxCapturePoints)
                    {
                        blueFlagPoints = maxCapturePoints;
                    }
                    else
                    {
                        blueFlagPoints += Time.deltaTime * flagCaptureSpeed;
                    }
                }
            }
        }
    }
     
    private void OnTriggerStay(Collider collider)
    {
        if (collider.gameObject.name == "Player1" || collider.gameObject.name == "Player1(Clone)")
        {
            bluePresent = true;
        }

        else if (collider.gameObject.name == "Player2" || collider.gameObject.name == "Player2(Clone)")
        {
            redPresent = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "Player1" || other.gameObject.name == "Player1(Clone)")
        {
            bluePresent = false;
        }

        else if (other.gameObject.name == "Player2" || other.gameObject.name == "Player2(Clone)")
        {
            redPresent = false;
        }
    }

    IEnumerator WhitePointsUpdate()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.1f);

            if (!redPresent && !bluePresent)
            {
                if (captureColour == "NULL")
                {
                    //cap blue
                    //uncap red or white
                    if (blueFlagPoints > 0)
                    {
                        blueFlagPoints -= Time.deltaTime * flagCaptureSpeed;
                        //-= blue
                    }
                    else if (redFlagPoints > 0)
                    {
                        redFlagPoints -= Time.deltaTime * flagCaptureSpeed;
                        //-= red
                    }
                    else
                    {
                        if (neutralFlagPoints < maxCapturePoints)
                        {
                            print("white");
                            if (neutralFlagPoints > maxCapturePoints)
                            {
                                neutralFlagPoints = maxCapturePoints;
                            }
                            else
                            {
                                neutralFlagPoints += Time.deltaTime * flagCaptureSpeed;
                            }
                        }
                    }
                }
            }

        }
    }

    IEnumerator AddScoreToteamColour()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.5f);

            if (captureColour == "Blue")
            {
                scoreManagerObject.GetComponent<ScoreManager>().UpdateKingPoints("Blue",1);
            }


            if (captureColour == "Red")
            {
                scoreManagerObject.GetComponent<ScoreManager>().UpdateKingPoints("Red", 1);
            }
        }
    }
}
