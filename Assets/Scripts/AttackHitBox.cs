﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackHitBox : MonoBehaviour
{
    public int damage = 0;
    public string teamColour = "NULL";

    public void Start()
    {
        Destroy(this.gameObject, 0.05f);
    }

    public void SetAttackvariables(int AttackDamage, string colour)
    {
        damage = AttackDamage;
        teamColour = colour;
    }

}
