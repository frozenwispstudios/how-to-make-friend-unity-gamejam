﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deatheffects : MonoBehaviour
{
    private void Start()
    {
        StartCoroutine(killobject());
    }

    IEnumerator killobject()
    {
        yield return new WaitForSeconds(3f);
        Destroy(this.gameObject);
    }
}
