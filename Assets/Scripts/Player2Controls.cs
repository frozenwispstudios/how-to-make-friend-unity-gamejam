﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Player2Controls : MonoBehaviour
{
    public float speed = 6f;

    Animator anim;
    Rigidbody rb;
    Vector3 movement;
    Vector3 oldDir;

    [SerializeField] int health = 3;
    [SerializeField] string teamColour = "Red";

    #region Attack Variables
    [SerializeField] private Transform attackSpawnTransform;
    [SerializeField] private GameObject attackHitBox;
    [SerializeField] private int Attackdamage = 1;
    [SerializeField] private GameObject Attackpoint;
    #endregion


    void Awake()
    {

        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        float h = Input.GetAxisRaw("Horizontal2");
        float v = Input.GetAxisRaw("Vertical2");

        MovePlayer(h, v);
        Animating(h, v);

        if (Input.GetButtonDown("Gather2"))
        {
            GameObject[] teamMembers = GameObject.FindGameObjectsWithTag("RedTeam");
            for (int i = 1; i < teamMembers.Count(); i++)
            {
                teamMembers[i].GetComponent<HumanAI>().Gather(this.gameObject);
            }
        }

        if (Input.GetButtonDown("Scatter2"))
        {
            GameObject[] teamMembers = GameObject.FindGameObjectsWithTag("RedTeam");
            for (int i = 1; i < teamMembers.Count(); i++)
            {
                teamMembers[i].GetComponent<HumanAI>().breakoff();
            }
        }

        if (Input.GetButtonDown("Melee2"))
        {
            anim.SetTrigger("Attack");
        }

        if (Input.GetButtonDown("Attack2"))
        {
            GameObject[] teamMembers = GameObject.FindGameObjectsWithTag("RedTeam");
            for (int i = 1; i < teamMembers.Count(); i++)
            {
                teamMembers[i].GetComponent<HumanAI>().AttackArea(Attackpoint, teamColour);
            }
        }


    }


    void MovePlayer(float h, float v)
    {
        // Set the movement vector based on the axis input.
        movement.Set(h, 0f, v);

        transform.Translate(movement * speed * Time.deltaTime, Space.World);

        if (h != 0f || v != 0f)
        {
            Vector3 lookDirection = new Vector3(h, 0, v);
            oldDir = lookDirection;
            transform.rotation = Quaternion.LookRotation(lookDirection);
        }
        else
        {
            if (movement != Vector3.zero)
            {
                transform.rotation = Quaternion.LookRotation(oldDir);
            }
        }

    }

    void Animating(float h, float v)
    {
        // Create a boolean that is true if either of the input axes is non-zero.
        bool walking = h != 0f || v != 0f;

        // Tell the animator whether or not the player is walking.
        anim.SetBool("IsWalking", walking);
    }

    void AttackEnd()
    {
        //create a attack instance
        GameObject damageHitBox = Instantiate(attackHitBox, attackSpawnTransform);
        damageHitBox.GetComponent<AttackHitBox>().SetAttackvariables(Attackdamage, teamColour);
    }

    //Change colour of human
    public void SetTeamColour(string colour)
    {
        teamColour = colour;
    }

    //death/coverting
    void UpdateDeath()
    {
        //expolode
        if (teamColour == "Red")
        {
            // Instantiate(redparticles, transform);
        }

        if (teamColour == "Blue")
        {
            // Instantiate(redparticles, transform);
        }

        StartCoroutine(Death());

    }

    IEnumerator Death()
    {
        yield return new WaitForSeconds(0.2f);
        Destroy(this.gameObject);
        GameObject.Find("Flag").GetComponent<FlagLogic>().redPresent = false;
    }

    //player dying
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "HitBox")
        {
            //print("hit aewdwaf");
            if (collision.gameObject.GetComponent<AttackHitBox>().teamColour != teamColour)
            {
                health -= collision.gameObject.GetComponent<AttackHitBox>().damage;
            }

            //if human then change colour
            if (health <= 0 && teamColour == "NULL")
            {
                SetTeamColour(collision.gameObject.GetComponent<AttackHitBox>().teamColour);
            }

            if (health <= 0 && teamColour != collision.gameObject.GetComponent<AttackHitBox>().teamColour)
            {
                UpdateDeath();
            }

            Destroy(collision.gameObject);
        }
    }

}
