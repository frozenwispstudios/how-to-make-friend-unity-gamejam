﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartSceneManager : MonoBehaviour
{
    public GameObject Player1NotReadyText;
    public GameObject Player2NotReadyText;

    public GameObject Player1ReadyText;
    public GameObject Player2ReadyText;

    bool Player1Ready = false;
    bool Player2Ready = false;

    // Start is called before the first frame update
    void Start()
    {
    
    }

    void SetReadyTexts()
    {
        if (Player1Ready)
        {
            Player1NotReadyText.SetActive(false);
            Player1ReadyText.SetActive(true);
        }
        if (!Player1Ready)
        {
            Player1NotReadyText.SetActive(true);
            Player1ReadyText.SetActive(false);
        }

        if (Player2Ready)
        {
            Player2NotReadyText.SetActive(false);
            Player2ReadyText.SetActive(true);
        }
        if (!Player2Ready)
        {
            Player2NotReadyText.SetActive(true);
            Player2ReadyText.SetActive(false);
        }
    }

    void CheckPlayersReady()
    {
        if (Player1Ready & Player2Ready)
        {
            SceneManager.LoadScene("GameScene");
        }
    }



    // Update is called once per frame
    void Update()
    {

        if (Input.GetButtonDown("Melee1"))
        {
            Player1Ready = !Player1Ready;
        }

        if (Input.GetButtonDown("Melee2"))
        {
            Player2Ready = !Player2Ready;
        }

        SetReadyTexts();
        CheckPlayersReady();
    }
}
