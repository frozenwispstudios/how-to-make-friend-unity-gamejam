﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class humanSpawnManager : MonoBehaviour
{
    private GameObject[] SpawnPoints;
    [SerializeField] GameObject humanUnit;
    [SerializeField] GameObject FasthumanUnit;
    [SerializeField] GameObject BighumanUnit;
    [SerializeField] GameObject Player1Unit;
    [SerializeField] GameObject Player2Unit;


    private void Awake()
    {
        SpawnPoints = GameObject.FindGameObjectsWithTag("SpawnPoint");
    }

    private void Start()
    {
        StartCoroutine(spawnHuman());
    }
    private void Update()
    {
       if (GameObject.Find("Player1") == null && GameObject.Find("Player1(Clone)") == null)
       {
            int randInt = Random.Range(0, SpawnPoints.Count());
            GameObject player1 = Instantiate(Player1Unit, SpawnPoints[randInt].transform);
            GameObject.Find("Player1 Camera").GetComponent<CameraFollow>().target = player1.transform;
            GameObject.Find("Player1 Camera").transform.position = GameObject.Find("Player1CameraOffset").transform.position; 
       }

       if (GameObject.Find("Player2") == null && GameObject.Find("Player2(Clone)") == null)
       {
            int randInt = Random.Range(0, SpawnPoints.Count());
            GameObject player2 = Instantiate(Player2Unit, SpawnPoints[randInt].transform);
            GameObject.Find("Player2 Camera").GetComponent<CameraFollow>().target = player2.transform;
            GameObject.Find("Player2 Camera").transform.position = GameObject.Find("Player2CameraOffset").transform.position;
       }

    }

    //Chance returns true if with in percent given
    bool Chance(float percent)
    {
        return Random.Range(0, 100) <= percent;
    }

    IEnumerator spawnHuman()
    {
        //while timecount > 0
        while(true)
        {
            yield return new WaitForSeconds(2f);
            int randInt = Random.Range(0, SpawnPoints.Count());

            if (Chance(20))
            {
                Instantiate(BighumanUnit, SpawnPoints[randInt].transform);
            }
            if (Chance(25))
            {
                Instantiate(FasthumanUnit, SpawnPoints[randInt].transform);
            }

            else
            {
                Instantiate(humanUnit, SpawnPoints[randInt].transform);
            }
        }
    }
}
