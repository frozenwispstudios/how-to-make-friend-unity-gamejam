﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player1AttackPointManager : MonoBehaviour
{

    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        if (other.name != "Player1" && other.name != "Player1(Clone)" && other.tag == "BlueTeam")
        {
            Destroy(this.gameObject);
        }
    }
}
